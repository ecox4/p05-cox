//
//  PlayScene.m
//  p05-cox
//
//  Created by Em on 3/27/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "PlayScene.h"
#import "HelloScene.h"
#import "Universe.h"
@interface PlayScene()
@property BOOL contentCreated;
@property BOOL gameOver;
@property int score;
//when hits > 5, game over
@property int hits;
@end

@implementation PlayScene{
    //sprite objs
    SKSpriteNode *play;
    SKSpriteNode *danger;
}
-(void)didMoveToView:(SKView *)view{
    if(!self.contentCreated){
        [self createSceneContents];
        self.contentCreated = YES;
        self.gameOver = NO;
        self.hits = 0;
        self.score = 0;
        self.backgroundColor = [SKColor grayColor];
    }
}
-(void)createSceneContents{
    self.scaleMode = SKSceneScaleModeAspectFill;
    [self addChild:[self setBg]];
    for(int i = 0; i < 10; ++i){
        [self addChild:[self setPlayer:i]];
    }
    for(int i = 0; i < 5; ++i){
        [self addChild:[self makeDanger:i]];
    }
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if(self.gameOver){
        //return to hello
        SKAction *remove = [SKAction removeFromParent];
        for(int i = 1; i < 10; ++i){
            NSString *str = [NSString stringWithFormat:@"play_%d",i];
            SKNode *pNodes = [self childNodeWithName:str];
            if(pNodes != nil){
                pNodes.name = nil;
                [pNodes runAction:remove];
            }
        }
        for(int i = 0; i < 5; ++i){
            NSString *str = [NSString stringWithFormat:@"danger_%d",i];
            SKNode *dNode = [self childNodeWithName:str];
            if(dNode != nil){
                dNode.name = nil;
                [dNode runAction:remove];
            }
        }
        SKNode *pNode = [self childNodeWithName:@"play_0"];
        SKNode *bNode = [self childNodeWithName:@"bg"];
        if(pNode != nil){
            pNode.name = nil;
            bNode.name = nil;
            [pNode runAction:remove];
            [bNode runAction:remove];
            [pNode runAction:remove completion:^{
                SKScene *initScene = [[HelloScene alloc] initWithSize:self.size];
                SKTransition *tr = [SKTransition crossFadeWithDuration:0.5];
                [self.view presentScene:initScene transition:tr];
            }];
        }
        self.gameOver = NO;
        NSLog(@"Game Over");
    }else{
        if(self.hits == 4){
            self.gameOver = YES;
        }
        UITouch *touch = [touches anyObject];
        CGPoint loc = [touch locationInNode:self];
        SKNode *node = [self nodeAtPoint:loc];
        if(!([node.name rangeOfString:@"danger_"].location == NSNotFound)){
            self.hits++;
            int x = rand()%(int)CGRectGetMaxX(self.frame);
            int y = rand()%(int)CGRectGetMaxY(self.frame);
            node.position = CGPointMake(x, y);
            NSLog(@"Hit danger block.");
        }else if(!([node.name rangeOfString:@"play_"].location == NSNotFound)){
            self.score += 25;
            int x = rand()%(int)CGRectGetMaxX(self.frame);
            int y = rand()%(int)CGRectGetMaxY(self.frame);
            node.position = CGPointMake(x, y);
            NSLog(@"Hit normal block.");
        }
   }
}
-(SKSpriteNode *)setBg{
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"500.png"];
    bg.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    bg.name = @"bg";
    return bg;
}
-(SKSpriteNode *)makeDanger:(int)pos{
    danger = [SKSpriteNode spriteNodeWithImageNamed:@"bubble.png"];
    //randomize position and generate 5
    int x = rand()%(int)CGRectGetMaxX(self.frame);
    int y = rand()%(int)CGRectGetMaxY(self.frame);
    danger.position = CGPointMake(x, y);
    danger.physicsBody.categoryBitMask = dangerCat;
    NSString *str = [NSString stringWithFormat:@"danger_%d",pos];
    danger.name = str;
    return danger;
}
-(SKSpriteNode *)setPlayer:(int)pos{
    play = [SKSpriteNode spriteNodeWithImageNamed:@"bubble.png"];
    //randomize position and generate 10
    int x = rand()%(int)CGRectGetMaxX(self.frame);
    int y = rand()%(int)CGRectGetMaxY(self.frame);
    play.position = CGPointMake(x, y);
    play.physicsBody.categoryBitMask = playCat;
    NSString *str = [NSString stringWithFormat:@"play_%d",pos];
    play.name = str;
    return play;
}
@end
