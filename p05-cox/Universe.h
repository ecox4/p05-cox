//
//  Universe.h
//  p05-cox
//
//  Created by Em on 3/28/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import <Foundation/Foundation.h>

//implemented using professor's code in cs441-autolayout project

@interface Universe : NSObject

+(Universe *)sharedInstance;
@property (nonatomic) int counter;

-(void)saveState;
-(void)loadState;

@end
