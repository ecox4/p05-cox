//
//  helloScene.m
//  p05-cox
//
//  Created by Em on 3/27/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "PlayScene.h"
#import "Universe.h"
@interface HelloScene()
@property BOOL contentCreated;
@end

@implementation HelloScene

-(void)didMoveToView:(SKView *)view{
    if(!self.contentCreated){
        [self createSceneContents];
        self.contentCreated = YES;
    }
}
-(void)createSceneContents{
    self.backgroundColor = [SKColor darkGrayColor];
    self.scaleMode = SKSceneScaleModeAspectFill;
    [self addChild:[self newHelloNode]];
//   [self addChild:[self displayScore]];
}

-(SKLabelNode *)newHelloNode{
    SKLabelNode *helloNode = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue"];
    helloNode.text = @"press to start";
    helloNode.fontSize = 38;
    helloNode.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    helloNode.name = @"helloScreen";
    return helloNode;
}
-(SKLabelNode *)displayScore{
    SKLabelNode *scoreNode = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue"];
    scoreNode.text = [NSString stringWithFormat:@"score: %d",[[Universe sharedInstance] counter]];
    scoreNode.fontSize = 20;
    scoreNode.position = CGPointMake(CGRectGetMidX(self.frame),100);
    scoreNode.name = @"scoreNode";
    return scoreNode;
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    SKNode *helloNode = [self childNodeWithName:@"helloScreen"];
    SKNode *scoreNode = [self childNodeWithName:@"scoreNode"];
    if(helloNode != nil){
        helloNode.name = nil;
        SKAction *fade = [SKAction fadeOutWithDuration:0.5];
        SKAction *remove = [SKAction removeFromParent];
        SKAction *changeSequence = [SKAction sequence:@[fade,remove]];
        [helloNode runAction:changeSequence];
        [scoreNode runAction:changeSequence];
        [helloNode runAction:changeSequence completion:^{
            SKScene *game = [[PlayScene alloc] initWithSize:self.size];
            SKTransition *tr = [SKTransition crossFadeWithDuration:0.5];
            [self.view presentScene:game transition:tr];
        }];
    }
}

@end
