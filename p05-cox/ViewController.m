//
//  ViewController.m
//  p05-cox
//
//  Created by Em on 3/21/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "ViewController.h"
#import "HelloScene.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    SKView *spriteView = (SKView *)self.view;
    spriteView.showsDrawCount = YES;
    spriteView.showsFPS = YES;
    spriteView.showsNodeCount = YES;
    spriteView.showsPhysics = YES;
    
}

-(void)viewWillAppear:(BOOL)animated{
    HelloScene * hello = [[HelloScene alloc] initWithSize:CGSizeMake(800, 1200)];
    SKView *spriteView = (SKView *)self.view;
    [spriteView presentScene:hello];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
