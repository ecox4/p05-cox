//
//  main.m
//  p05-cox
//
//  Created by Em on 3/21/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
