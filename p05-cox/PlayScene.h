//
//  PlayScene.h
//  p05-cox
//
//  Created by Em on 3/27/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "HelloScene.h"
@interface PlayScene : SKScene

@end

static const uint32_t dangerCat = 0x1 <<0;
static const uint32_t playCat = 0x1 <<1;
